using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            venda.StatusVenda = 0;
            _context.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("Procurar/{id}")]
        public async Task<ActionResult<Venda>> Procurar(int id)
        {
        
            var venda = await _context.Vendas
             .Include(i=> i.Vendedor)
             .Where(i=> i.Vendedor.Id == id)
             .Include(i=> i.ListaProdutos)
             .Where(i=> i.Id == id)
             .FirstOrDefaultAsync();            

            return Ok(venda);
        }

        [HttpPut("Atualizar/{id}")]
        public async Task<ActionResult<Venda>> Atualizar(int id, EnumStatusVenda status)
        {
        
            var venda = await _context.Vendas
             .Include(i=> i.Vendedor)
             .Where(i=> i.Vendedor.Id == id)
             .Include(i=> i.ListaProdutos)
             .Where(i=> i.Id == id)
             .FirstOrDefaultAsync();

             if (venda.StatusVenda == EnumStatusVenda.AguardandoPagamento && (status == EnumStatusVenda.PagamentoAprovado || status == EnumStatusVenda.Cancelada))
             {
                venda.StatusVenda = status;
            }
            else if (venda.StatusVenda == EnumStatusVenda.PagamentoAprovado && (status == EnumStatusVenda.EnviadoParaTransportadora || status == EnumStatusVenda.Cancelada))
            {
                venda.StatusVenda = status;
            }
            else if (venda.StatusVenda == EnumStatusVenda.EnviadoParaTransportadora && status == EnumStatusVenda.Entregue)
            {
                venda.StatusVenda = status;
            }
            else 
            {
                return BadRequest(new { Erro = "Troca de status não permitida, verifique as transições de status permitidas" });
            }
             

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }
        
    }
}
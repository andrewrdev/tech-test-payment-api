using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Venda
    {   
        [Required]
        public int Id { get; set; }
        [Required]
        public Vendedor Vendedor { get; set; }
        [Required]
        public DateTime Data { get; set; }
        [Required]
        public List<Produto> ListaProdutos { get; set; }
        [Required]
        public EnumStatusVenda StatusVenda { get; set; }
        
    }
}